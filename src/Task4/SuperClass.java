package Task4;

public class SuperClass {
	public char symbol;

	public SuperClass(char symbol) {
		this.symbol = symbol;
	}

	public SuperClass copy() {
		return new SuperClass(symbol);
	}
}
