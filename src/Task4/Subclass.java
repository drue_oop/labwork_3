package Task4;

public class Subclass extends SuperClass {
	public String string;

	public Subclass( char symbol, String string) {
		super(symbol);
		this.string = string;
	}

	public Subclass copy() {
		return new Subclass(symbol, string);
	}
}
