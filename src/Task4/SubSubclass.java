package Task4;

public class SubSubclass extends Subclass {
	public int number;

	public SubSubclass(char symbol, String string, int number) {
		super(symbol, string);
		this.number = number;
	}

	public SubSubclass copy() {
		return new SubSubclass(symbol, string, number);
	}
}
