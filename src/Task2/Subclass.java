package Task2;

public class Subclass extends SuperClass {
	public int number;

	public Subclass(String value, int number) {
		super(value);
		this.number = number;
	}

	@Override
	public void setValue() {
		super.setValue();
		this.number = 0;
	}
	@Override
	public void setValue(String value) {
		super.setValue(value);
	}
	public void setValue(int number) {
		this.number = number;
	}
	public void setValue(String value, int number) {
		super.setValue(value);
		this.number = number;
	}
}
