package Task2;

public class SuperClass {
	private String value;

	public SuperClass(String value) {
		this.value = value;
	}

	public void setValue() {
		this.value = "";
	}
	public void setValue(String value) {
		this.value = value;
	}

	public int getLength() {
		return value.length();
	}
}
