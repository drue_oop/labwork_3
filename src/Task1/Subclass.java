package Task1;

public class Subclass extends SuperClass {
	private String value_2;

	public Subclass(String value) {
		super(value);
		this.value_2 = value;
	}

	public Subclass(String value, String value_2) {
		super(value);
		this.value_2 = value_2;
	}

	@Override
	public String toString() {
		return super.toString() + " value_2:" + this.value_2;
	}
}
