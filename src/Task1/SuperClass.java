package Task1;

public class SuperClass {
	private String value;

	public SuperClass(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return this.getClass().getName() + " value:" + value;
	}
}
