package Task5;

public class SuperClass {
	private String string;

	public SuperClass(String string) {
		this.string = string;
	}

	public void printInfo() {
		System.out.print(this.getClass().getName() + "\n	string: " + string + "\n");
	}
}
