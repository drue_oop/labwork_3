package Task5;

public class FirstSubclass extends SuperClass {
	private int number;

	public FirstSubclass(String string, int number) {
		super(string);
		this.number = number;
	}

	@Override
	public void printInfo() {
		super.printInfo();
		System.out.print("	number: " + number + "\n");
	}
}
