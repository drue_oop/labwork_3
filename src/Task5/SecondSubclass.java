package Task5;

public class SecondSubclass extends SuperClass {
	private char symbol;

	public SecondSubclass(String string, char symbol) {
		super(string);
		this.symbol = symbol;
	}

	@Override
	public void printInfo() {
		super.printInfo();
		System.out.print("	symbol: " + symbol + "\n");
	}
}
