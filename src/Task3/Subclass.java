package Task3;

public class Subclass extends SuperClass {
	public char symbol;

	public Subclass(int number, char symbol) {
		super(number);
		this.symbol = symbol;
	}

	public void setValue(int number, char symbol) {
		super.setValue(number);
		this.symbol = symbol;
	}

	@Override
	public String toString() {
		return super.toString() + " symbol:" + this.symbol;
	}
}
