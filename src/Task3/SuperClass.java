package Task3;

public class SuperClass {
	public int number;

	public SuperClass(int number) {
		this.number = number;
	}

	public void setValue(int number) {
		this.number = number;
	}

	@Override
	public String toString() {
		return this.getClass().getName() + " number:" + number;
	}
}
