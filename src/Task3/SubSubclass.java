package Task3;

public class SubSubclass extends Subclass {
	public String string;

	public SubSubclass(int number, char symbol, String string) {
		super(number, symbol);
		this.string = string;
	}

	public void setValue(int number, char symbol, String string) {
		super.setValue(number, symbol);
		this.string = string;
	}

	@Override
	public String toString() {
		return super.toString() + " string:" + this.string;
	}
}
